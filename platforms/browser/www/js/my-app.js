var app = new Framework7({
    // App root element
    root: '#app',
    // App Name
    name: 'My App',
    // App id
    id: 'com.myapp.test',
    // Enable swipe panel
    panel: {
        swipe: 'left',
    },
    // Add default routes
    routes: [
        {
            path: '/about/',
            url: 'about.html',
        },
        {
            name: 'message_new',
            path: '/message_new/',
            url: './message_new',
        },
    ],
    // ... other parameters
});

var $$ = Dom7;

